package com.interview.marvelapp.data.local.db;

import com.interview.marvelapp.data.model.db.Character;

import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AppDbHelper implements DbHelper{

    private final AppDatabase mAppDatabase;
    private final Executor mExecutor;

    @Inject
    public AppDbHelper(AppDatabase appDatabase, Executor executor) {
        this.mAppDatabase = appDatabase;
        this.mExecutor = executor;
    }

    @Override
    public void insertCharacter(Character character) {
        mExecutor.execute(() -> mAppDatabase.getCharacterDAO().insert(character));
    }
}