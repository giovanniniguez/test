package com.interview.marvelapp.data.local.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.interview.marvelapp.data.local.db.dao.CharacterDAO;
import com.interview.marvelapp.data.model.db.Character;

@Database(entities = {Character.class}, version = 1)
@TypeConverters(ThumbnailConverter.class)
public abstract class AppDatabase extends RoomDatabase {

    public abstract CharacterDAO getCharacterDAO();
}