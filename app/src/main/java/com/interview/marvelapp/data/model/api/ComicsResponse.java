package com.interview.marvelapp.data.model.api;

import com.interview.marvelapp.data.model.db.Comic;

public class ComicsResponse {

    private int code;
    private DataResponse<Comic> data;

    public ComicsResponse() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataResponse getDataResponse() {
        return data;
    }

    public void setDataResponse(DataResponse data) {
        this.data = data;
    }
}
