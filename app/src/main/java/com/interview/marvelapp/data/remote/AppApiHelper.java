package com.interview.marvelapp.data.remote;

import com.interview.marvelapp.BuildConfig;
import com.interview.marvelapp.data.model.api.CharactersResponse;
import com.interview.marvelapp.data.model.api.ComicsResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class AppApiHelper implements ApiHelper {

    @Inject
    public AppApiHelper() {
    }

    @Override
    public Observable<CharactersResponse> getCharactersResponse(String limit, String offset) {
        return Rx2AndroidNetworking
                .get(ApiEndPoint.ENDPOINT_CHARACTERS)
                .addQueryParameter("apikey", BuildConfig.PUBLIC_KEY)
                .addQueryParameter("hash", BuildConfig.API_HASH)
                .addQueryParameter("ts", "1")
                .addQueryParameter("limit", limit)
                .addQueryParameter("offset", offset)
                .build()
                .getObjectObservable(CharactersResponse.class);
    }

    @Override
    public Observable<ComicsResponse> getComicsResponse(String limit, String offset, int characterId) {
        return Rx2AndroidNetworking
                .get(ApiEndPoint.ENDPOINT_CHARACTERS + "/" + characterId + "/comics")
                .addQueryParameter("apikey", BuildConfig.PUBLIC_KEY)
                .addQueryParameter("hash", BuildConfig.API_HASH)
                .addQueryParameter("ts", "1")
                .addQueryParameter("limit", limit)
                .addQueryParameter("offset", offset)
                .build()
                .getObjectObservable(ComicsResponse.class);
    }
}
