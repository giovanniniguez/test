package com.interview.marvelapp.data.local.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;

import com.interview.marvelapp.data.model.db.Character;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface CharacterDAO {

    @Insert(onConflict = REPLACE)
    long insert(Character character);
}
