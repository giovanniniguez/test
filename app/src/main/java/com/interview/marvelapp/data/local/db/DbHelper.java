package com.interview.marvelapp.data.local.db;

import com.interview.marvelapp.data.model.db.Character;

public interface DbHelper {

    void insertCharacter(Character character);
}
