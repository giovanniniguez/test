package com.interview.marvelapp.data;

import com.interview.marvelapp.data.local.db.DbHelper;
import com.interview.marvelapp.data.model.api.CharactersResponse;
import com.interview.marvelapp.data.model.api.ComicsResponse;
import com.interview.marvelapp.data.model.db.Character;
import com.interview.marvelapp.data.remote.ApiHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class AppDataManager implements DataManager {

    private final ApiHelper apiHelper;

    private final DbHelper dbHelper;

    @Inject
    public AppDataManager(ApiHelper apiHelper, DbHelper dbHelper) {
        this.apiHelper = apiHelper;
        this.dbHelper = dbHelper;
    }

    @Override
    public Observable<CharactersResponse> getCharactersResponse(String limit, String offset) {
        return apiHelper.getCharactersResponse(limit, offset);
    }

    @Override
    public Observable<ComicsResponse> getComicsResponse(String limit, String offset, int characterId) {
        return apiHelper.getComicsResponse(limit, offset, characterId);
    }

    @Override
    public void insertCharacter(Character character) {
        dbHelper.insertCharacter(character);
    }
}
