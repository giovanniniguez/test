package com.interview.marvelapp.data;

import com.interview.marvelapp.data.local.db.DbHelper;
import com.interview.marvelapp.data.remote.ApiHelper;

public interface DataManager extends ApiHelper, DbHelper{
}
