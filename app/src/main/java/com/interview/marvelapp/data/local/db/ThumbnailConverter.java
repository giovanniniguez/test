package com.interview.marvelapp.data.local.db;

import android.arch.persistence.room.TypeConverter;

import com.interview.marvelapp.data.model.db.Thumbnail;

public class ThumbnailConverter {

    @TypeConverter
    public static Thumbnail toThumbnail(String pathAndExtension) {
        return new Thumbnail(pathAndExtension.split("|@|")[0], pathAndExtension.split("|@|")[1]);
    }

    @TypeConverter
    public static String fromThumbnail(Thumbnail thumbnail) {
        return thumbnail.getPath() + "|@|" + thumbnail.getExtension();
    }
}
