package com.interview.marvelapp.data.model.api;

import com.interview.marvelapp.data.model.db.Character;

public class CharactersResponse {

    private int code;
    private DataResponse<Character> data;

    public CharactersResponse() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataResponse getDataResponse() {
        return data;
    }

    public void setDataResponse(DataResponse data) {
        this.data = data;
    }
}