package com.interview.marvelapp.data.remote;

import com.interview.marvelapp.BuildConfig;

public final class ApiEndPoint {

    public static final String ENDPOINT_CHARACTERS = BuildConfig.BASEURL + "characters";

    private ApiEndPoint() {
    }
}
