package com.interview.marvelapp.data.remote;

import com.interview.marvelapp.data.model.api.CharactersResponse;
import com.interview.marvelapp.data.model.api.ComicsResponse;

import io.reactivex.Observable;

public interface ApiHelper {

    Observable<CharactersResponse> getCharactersResponse(String limit, String offset);
    Observable<ComicsResponse> getComicsResponse(String limit, String offset, int characterId);
}
