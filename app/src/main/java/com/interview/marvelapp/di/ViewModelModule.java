package com.interview.marvelapp.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.interview.marvelapp.ui.characterdetail.DetailViewModel;
import com.interview.marvelapp.ui.characters.CharactersViewModel;
import com.interview.marvelapp.utils.ViewModelProviderFactory;

import dagger.Module;

import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public class ViewModelModule {

    @Provides
    ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory viewModelFactory) {
        return viewModelFactory;
    }

    @Provides
    @IntoMap
    @ViewModelKey(CharactersViewModel.class)
    ViewModel charactersViewModel(CharactersViewModel charactersViewModel) {
        return charactersViewModel;
    }

    @Provides
    @IntoMap
    @ViewModelKey(DetailViewModel.class)
    ViewModel detailViewModel(DetailViewModel detailViewModel) {
        return detailViewModel;
    }

}
