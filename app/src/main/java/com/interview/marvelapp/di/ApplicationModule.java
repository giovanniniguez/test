package com.interview.marvelapp.di;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.interview.marvelapp.BuildConfig;
import com.interview.marvelapp.data.AppDataManager;
import com.interview.marvelapp.data.DataManager;
import com.interview.marvelapp.data.local.db.AppDatabase;
import com.interview.marvelapp.data.local.db.AppDbHelper;
import com.interview.marvelapp.data.local.db.DbHelper;
import com.interview.marvelapp.data.remote.ApiHelper;
import com.interview.marvelapp.data.remote.AppApiHelper;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    Executor provideExecutor() {
        return Executors.newSingleThreadExecutor();
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(Context context) {
        return Room
                .databaseBuilder(context, AppDatabase.class, BuildConfig.DBNAME)
                .fallbackToDestructiveMigration()
                .build();
    }
}
