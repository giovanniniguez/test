package com.interview.marvelapp.di;

import com.interview.marvelapp.ui.characterdetail.DetailFragment;
import com.interview.marvelapp.ui.characters.CharactersListFragment;
import com.interview.marvelapp.ui.splash.Splash;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, ViewModelModule.class})
public interface ApplicationComponent {

    void inject(Splash target);

    void inject(CharactersListFragment target);

    void inject(DetailFragment target);
}