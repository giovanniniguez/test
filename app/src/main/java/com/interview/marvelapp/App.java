package com.interview.marvelapp;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.gsonparserfactory.GsonParserFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.interview.marvelapp.data.model.db.Thumbnail;
import com.interview.marvelapp.data.remote.CustomDeserializer;
import com.interview.marvelapp.di.ApplicationComponent;
import com.interview.marvelapp.di.ApplicationModule;
import com.interview.marvelapp.di.DaggerApplicationComponent;
import com.interview.marvelapp.di.ViewModelModule;

public class App extends Application {

    private ApplicationComponent component;

    public ApplicationComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .viewModelModule(new ViewModelModule())
                .build();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Thumbnail.class, new CustomDeserializer<Thumbnail>())
                .create();

        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.setParserFactory(new GsonParserFactory(gson));
    }
}
