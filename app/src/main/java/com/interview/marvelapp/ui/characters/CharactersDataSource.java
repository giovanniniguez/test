package com.interview.marvelapp.ui.characters;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.interview.marvelapp.data.DataManager;
import com.interview.marvelapp.data.model.db.Character;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CharactersDataSource extends PageKeyedDataSource<Long, Character> {

    private DataManager mDataManager;
    private CompositeDisposable mCompositeDisposable;
    private MutableLiveData loading;


    public CharactersDataSource(DataManager mDataManager, CompositeDisposable compositeDisposable, MutableLiveData loading) {
        this.mDataManager = mDataManager;
        this.mCompositeDisposable = compositeDisposable;
        this.loading = loading;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<Long, Character> callback) {

        loading.postValue(true);

        mCompositeDisposable.add(mDataManager.getCharactersResponse(String.valueOf(params.requestedLoadSize), String.valueOf(0))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        object -> {
                            loading.postValue(false);
                            callback.onResult(object.getDataResponse().getResults(), null, 2L);
                        },
                        Throwable -> {
                            loading.postValue(false);
                            Throwable.printStackTrace();
                        },
                        () -> {
                        }
                ));
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, Character> callback) {
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, Character> callback) {

        mCompositeDisposable.add(mDataManager.getCharactersResponse(String.valueOf(params.requestedLoadSize), String.valueOf((params.key - 1) * params.requestedLoadSize))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        object -> callback.onResult(object.getDataResponse().getResults(), params.key + 1),
                        Throwable::printStackTrace,
                        () -> {
                        }
                ));
    }
}
