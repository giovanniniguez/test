package com.interview.marvelapp.ui.splash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.interview.marvelapp.R;
import com.interview.marvelapp.ui.characters.CharactersActivity;
import com.interview.marvelapp.utils.Constants;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Animation pulse = AnimationUtils.loadAnimation(Splash.this, R.anim.pulse);
        findViewById(R.id.ivLogo).startAnimation(pulse);

        new Handler().postDelayed(() -> {
            Intent i = new Intent(Splash.this, CharactersActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }, Constants.SPLASH_DELAY);
    }
}
