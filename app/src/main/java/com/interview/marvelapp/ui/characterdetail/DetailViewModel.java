package com.interview.marvelapp.ui.characterdetail;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.interview.marvelapp.data.DataManager;
import com.interview.marvelapp.data.model.db.Comic;
import com.interview.marvelapp.ui.base.BaseViewModel;

import javax.inject.Inject;

public class DetailViewModel extends BaseViewModel {

    private ComicsDataSourceFactory comicsDataSourceFactory;
    private LiveData<PagedList<Comic>> comicsLiveData;
    private MutableLiveData loading;

    @Inject
    public DetailViewModel(DataManager dataManager) {
        super(dataManager);
        loading = new MutableLiveData();
    }

    public MutableLiveData getLoading() {
        return loading;
    }

    public LiveData<PagedList<Comic>> getComicsLiveDataForCharacterId(int pageSize, int characterId) {
        comicsDataSourceFactory = new ComicsDataSourceFactory(getDataManager(), getCompositeDisposable(), characterId, loading);
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setInitialLoadSizeHint(pageSize)
                        .setPageSize(pageSize)
                        .build();
        comicsLiveData = (new LivePagedListBuilder(comicsDataSourceFactory, pagedListConfig)).build();
        return comicsLiveData;
    }
}
