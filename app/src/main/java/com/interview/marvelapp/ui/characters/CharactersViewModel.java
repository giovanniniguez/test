package com.interview.marvelapp.ui.characters;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.interview.marvelapp.data.DataManager;
import com.interview.marvelapp.data.model.db.Character;
import com.interview.marvelapp.ui.base.BaseViewModel;

import javax.inject.Inject;

public class CharactersViewModel extends BaseViewModel {

    private CharactersDataSourceFactory charactersDataSourceFactory;
    private LiveData<PagedList<Character>> charactersLiveData;
    private MutableLiveData loading;

    @Inject
    public CharactersViewModel(DataManager dataManager) {
        super(dataManager);
        loading = new MutableLiveData();
    }

    public MutableLiveData getLoading() {
        return loading;
    }

    public LiveData<PagedList<Character>> getCharactersLiveData(int pageSize) {
        charactersDataSourceFactory = new CharactersDataSourceFactory(getDataManager(), getCompositeDisposable(), loading);
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setInitialLoadSizeHint(pageSize)
                        .setPageSize(pageSize)
                        .build();
        charactersLiveData = (new LivePagedListBuilder(charactersDataSourceFactory, pagedListConfig)).build();
        return charactersLiveData;
    }
}
