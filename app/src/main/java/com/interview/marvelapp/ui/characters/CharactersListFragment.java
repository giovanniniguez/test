package com.interview.marvelapp.ui.characters;

import android.arch.lifecycle.ViewModelProviders;

import android.content.Intent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.interview.marvelapp.App;
import com.interview.marvelapp.R;
import com.interview.marvelapp.ui.characterdetail.CharacterDetailActivity;
import com.interview.marvelapp.utils.ViewModelProviderFactory;

import java.util.Objects;

import javax.inject.Inject;

public class CharactersListFragment extends Fragment {

    @Inject
    ViewModelProviderFactory viewModelFactory;
    CharactersViewModel charactersViewModel;

    private CharactersAdapter charactersAdapter;
    private RecyclerView rvCharacters;
    private RecyclerView.LayoutManager layoutManager;

    private int pageSize;

    public CharactersListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((App) Objects.requireNonNull(getActivity()).getApplication()).getComponent().inject(this);

        charactersViewModel = ViewModelProviders.of(this, viewModelFactory).get(CharactersViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_characters_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getResources().getBoolean(R.bool.phone_portrait_only)) {
            pageSize = 10;
            layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        } else {
            pageSize = 20;
            layoutManager = new GridLayoutManager(getActivity(), 3);
        }

        rvCharacters = view.findViewById(R.id.rvCharacters);

        rvCharacters.setLayoutManager(layoutManager);

        charactersAdapter = new CharactersAdapter(getActivity(), (id, image, description, name) -> {
            Intent i = new Intent(getActivity(), CharacterDetailActivity.class);
            i.putExtra("id", id);
            i.putExtra("image", image);
            i.putExtra("description", description);
            i.putExtra("name", name);
            startActivity(i);
        });
        rvCharacters.setAdapter(charactersAdapter);
        charactersViewModel.getCharactersLiveData(pageSize).observe(this, characters -> charactersAdapter.submitList(characters));
        charactersViewModel.getLoading().observe(this, o -> view.findViewById(R.id.pbLoading).setVisibility(((boolean) o) ? View.VISIBLE : View.GONE));
    }
}
