package com.interview.marvelapp.ui.characterdetail;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.graphics.Palette;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.interview.marvelapp.R;
import com.interview.marvelapp.data.model.db.Comic;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class ComicsAdapter extends PagedListAdapter<Comic, ComicsAdapter.ComicViewHolder> {

    private Context context;

    protected ComicsAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.context = context;
    }

    @NonNull
    @Override
    public ComicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_comic, parent, false);
        return new ComicViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ComicViewHolder holder, int position) {
        Comic currentComic = getItem(holder.getAdapterPosition());
        String imagePath = currentComic.getThumbnail().getPath() + "." + currentComic.getThumbnail().getExtension();
        Picasso.get().load(imagePath).into(holder.target);
        holder.tvName.setText(currentComic.getTitle());
    }

    class ComicViewHolder extends RecyclerView.ViewHolder implements Target {

        ImageView ivImage;
        TextView tvName;
        Target target;

        ComicViewHolder(View itemView) {
            super(itemView);
            this.ivImage = itemView.findViewById(R.id.ivImage);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.target = this;
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            this.ivImage.setImageBitmap(bitmap);
            Palette.from(bitmap).generate(palette -> {
                int vibrantColor = palette.getDarkVibrantColor(context.getResources().getColor(R.color.colorPrimaryDark));
                this.ivImage.setBackgroundColor(vibrantColor);
            });
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    }

    private static final DiffUtil.ItemCallback<Comic> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Comic>() {
                @Override
                public boolean areItemsTheSame(Comic oldItem, Comic newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(Comic oldItem, Comic newItem) {
                    return oldItem.equals(newItem);
                }
            };
}
