package com.interview.marvelapp.ui.characterdetail;

import android.arch.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.interview.marvelapp.App;
import com.interview.marvelapp.R;


import com.interview.marvelapp.utils.ViewModelProviderFactory;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import javax.inject.Inject;

public class DetailFragment extends Fragment {

    @Inject
    ViewModelProviderFactory viewModelFactory;
    DetailViewModel detailViewModel;

    private int id;
    private String image;
    private String description;
    private String name;

    private ImageView ivImage;
    private TextView tvName;
    private TextView tvDescription;
    private RecyclerView rvComics;
    private ComicsAdapter comicsAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public DetailFragment() {
    }

    public static DetailFragment newInstance(int id, String image, String description, String name) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putInt("id", id);
        args.putString("image", image);
        args.putString("description", description);
        args.putString("name", name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt("id");
            image = getArguments().getString("image");
            description = getArguments().getString("description");
            name = getArguments().getString("name");
        }

        ((App) Objects.requireNonNull(getActivity()).getApplication()).getComponent().inject(this);

        detailViewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailViewModel.class);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.ivBack).setOnClickListener(v -> Objects.requireNonNull(getActivity()).onBackPressed());

        ivImage = view.findViewById(R.id.ivImage);
        tvName = view.findViewById(R.id.tvName);
        tvDescription = view.findViewById(R.id.tvDescription);
        rvComics = view.findViewById(R.id.rvComics);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvComics.setLayoutManager(mLayoutManager);

        tvName.setText(name);
        tvDescription.setText(description.length() <= 0 ? getString(R.string.no_description) : description);
        Picasso.get().load(image).into(ivImage);

        comicsAdapter = new ComicsAdapter(getActivity());
        rvComics.setAdapter(comicsAdapter);
        detailViewModel.getComicsLiveDataForCharacterId(5, id).observe(this, comics -> comicsAdapter.submitList(comics));
        detailViewModel.getLoading().observe(this, o -> view.findViewById(R.id.pbLoading).setVisibility(((boolean) o) ? View.VISIBLE : View.GONE));
    }
}
