package com.interview.marvelapp.ui.characters;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.graphics.Bitmap;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.graphics.Palette;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.interview.marvelapp.R;
import com.interview.marvelapp.data.model.db.Character;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Objects;

public class CharactersAdapter extends PagedListAdapter<Character, CharactersAdapter.CharacterViewHolder> {

    private Context context;
    private OnItemClickListener onItemClickListener;

    public CharactersAdapter(Context context, OnItemClickListener onItemClickListener) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public CharacterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_character, parent, false);
        return new CharacterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterViewHolder holder, int position) {
        Character currentCharacter = getItem(holder.getAdapterPosition());
        holder.tvName.setText(Objects.requireNonNull(currentCharacter).getName());
        String imagePath = currentCharacter.getThumbnail().getPath() + "/detail." + currentCharacter.getThumbnail().getExtension();
        Picasso.get().load(imagePath).into(holder.target);
        holder.ivImage.setOnClickListener(v -> onItemClickListener.onClick(currentCharacter.getId(), imagePath, currentCharacter.getDescription(), currentCharacter.getName()));
    }

    class CharacterViewHolder extends RecyclerView.ViewHolder implements Target {

        Target target;
        TextView tvName;
        ImageView ivImage;
        ConstraintLayout clItem;

        CharacterViewHolder(View itemView) {
            super(itemView);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.ivImage = itemView.findViewById(R.id.ivImage);
            this.clItem = itemView.findViewById(R.id.clItem);
            this.target = this;
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            this.ivImage.setImageBitmap(bitmap);
            Palette.from(bitmap).generate(palette -> {
                int vibrantColor = palette.getDarkVibrantColor(context.getResources().getColor(R.color.colorPrimaryDark));
                this.ivImage.setBackgroundColor(vibrantColor);
            });
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    }

    private static final DiffUtil.ItemCallback<Character> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Character>() {
                @Override
                public boolean areItemsTheSame(Character oldItem, Character newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(Character oldItem, Character newItem) {
                    return oldItem.equals(newItem);
                }
            };

    public interface OnItemClickListener {
        void onClick(int id, String image, String description, String name);
    }
}
