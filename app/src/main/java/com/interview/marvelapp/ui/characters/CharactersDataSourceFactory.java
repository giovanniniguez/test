package com.interview.marvelapp.ui.characters;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.interview.marvelapp.data.DataManager;

import io.reactivex.disposables.CompositeDisposable;

public class CharactersDataSourceFactory extends DataSource.Factory {

    private DataManager dataManager;
    private CompositeDisposable compositeDisposable;
    private MutableLiveData loading;

    public CharactersDataSourceFactory(DataManager dataManager, CompositeDisposable compositeDisposable, MutableLiveData loading) {
        this.dataManager = dataManager;
        this.compositeDisposable = compositeDisposable;
        this.loading = loading;
    }

    @Override
    public DataSource create() {
        return new CharactersDataSource(this.dataManager, compositeDisposable, loading);
    }
}
