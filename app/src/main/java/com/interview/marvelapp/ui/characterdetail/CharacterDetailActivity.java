package com.interview.marvelapp.ui.characterdetail;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.interview.marvelapp.R;

public class CharacterDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_detail);

        if (getResources().getBoolean(R.bool.phone_portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.flDetail, DetailFragment.newInstance(
                        getIntent().getExtras().getInt("id"),
                        getIntent().getExtras().getString("image"),
                        getIntent().getExtras().getString("description"),
                        getIntent().getExtras().getString("name")))
                .commit();
    }
}
