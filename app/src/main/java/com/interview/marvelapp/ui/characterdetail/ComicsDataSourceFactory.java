package com.interview.marvelapp.ui.characterdetail;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.interview.marvelapp.data.DataManager;

import io.reactivex.disposables.CompositeDisposable;

public class ComicsDataSourceFactory extends DataSource.Factory {

    private DataManager dataManager;
    private CompositeDisposable compositeDisposable;
    private int characterId;
    private MutableLiveData loading;

    public ComicsDataSourceFactory(DataManager dataManager, CompositeDisposable compositeDisposable, int characterId, MutableLiveData loading) {
        this.dataManager = dataManager;
        this.compositeDisposable = compositeDisposable;
        this.characterId = characterId;
        this.loading = loading;
    }

    @Override
    public DataSource create() {
        return new ComicsDataSource(dataManager, compositeDisposable, characterId, loading);
    }
}
