package com.interview.marvelapp.ui.characterdetail;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.interview.marvelapp.data.DataManager;
import com.interview.marvelapp.data.model.db.Comic;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ComicsDataSource extends PageKeyedDataSource<Long, Comic> {

    private DataManager mDataManager;
    private CompositeDisposable mCompositeDisposable;
    private int characterId;
    private MutableLiveData loading;

    public ComicsDataSource(DataManager mDataManager, CompositeDisposable mCompositeDisposable, int characterId, MutableLiveData loading) {
        this.mDataManager = mDataManager;
        this.mCompositeDisposable = mCompositeDisposable;
        this.characterId = characterId;
        this.loading = loading;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<Long, Comic> callback) {

        loading.postValue(true);

        mCompositeDisposable.add(mDataManager.getComicsResponse(String.valueOf(params.requestedLoadSize), String.valueOf(0), characterId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        object -> {
                            loading.postValue(false);
                            callback.onResult(object.getDataResponse().getResults(), null, 2L);
                        },
                        Throwable -> {
                            loading.postValue(false);
                            Throwable.printStackTrace();
                        },
                        () -> {
                        }
                ));
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, Comic> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, Comic> callback) {
        mCompositeDisposable.add(mDataManager.getComicsResponse(String.valueOf(params.requestedLoadSize), String.valueOf((params.key - 1) * params.requestedLoadSize), characterId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        object -> callback.onResult(object.getDataResponse().getResults(), params.key + 1),
                        Throwable::printStackTrace,
                        () -> {
                        }
                ));
    }
}
