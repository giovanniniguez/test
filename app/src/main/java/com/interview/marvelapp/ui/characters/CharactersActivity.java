package com.interview.marvelapp.ui.characters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.interview.marvelapp.R;
import com.interview.marvelapp.ui.base.NoConnectionFragment;
import com.interview.marvelapp.utils.NetworkUtils;

public class CharactersActivity extends AppCompatActivity {

    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getSupportFragmentManager().executePendingTransactions();
            if (getSupportFragmentManager().findFragmentByTag("true") == null && NetworkUtils.isNetworkConnected(getApplicationContext())) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.flCharactersList, new CharactersListFragment(), String.valueOf(NetworkUtils.isNetworkConnected(getApplicationContext())))
                        .commit();
            } else if (getSupportFragmentManager().findFragmentByTag("false") == null && !NetworkUtils.isNetworkConnected(getApplicationContext())) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.flCharactersList, new NoConnectionFragment(), String.valueOf(NetworkUtils.isNetworkConnected(getApplicationContext())))
                        .commit();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characters);

        if (getResources().getBoolean(R.bool.phone_portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        Fragment fragment;

        if (NetworkUtils.isNetworkConnected(this))
            fragment = new CharactersListFragment();
        else
            fragment = new NoConnectionFragment();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.flCharactersList, fragment, String.valueOf(NetworkUtils.isNetworkConnected(this)))
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(networkChangeReceiver);
    }
}
